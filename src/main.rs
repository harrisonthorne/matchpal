use std::{cmp::Ordering, path::Path, str::FromStr};
use clap::Parser;
use image::{DynamicImage, ImageBuffer, Pixel};
use log::debug;
use palette::{ColorDifference, FromColor, Lab, Srgb, rgb::{Rgb, channels::Argb}, Hsl};
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};

type Palette = Vec<Lab>;

fn main() {
    use image::io::Reader as ImageReader;

    simplelog::SimpleLogger::init(log::LevelFilter::Off, simplelog::Config::default()).unwrap();

    debug!("parsing options");
    let opts = MatchpalOpts::parse();

    // get palette
    debug!("getting palette from '{}'", opts.palette);
    let palette = parse_palette(&opts.palette);

    // if no palette, stop
    if palette.is_empty() {
        eprintln!("palette is empty; exiting");
        return;
    }

    // change the palette of the image
    debug!("opening image '{}'", opts.input);
    let img = ImageReader::open(&opts.input).unwrap().decode().unwrap();

    debug!("changing palette");
    let new_rgb = change_palette(img, &palette, &opts);
    let new_img = DynamicImage::ImageRgb8(new_rgb);

    debug!("writing image to '{}'", opts.output);
    new_img.save(opts.output).unwrap();
}

fn change_palette(src_img: DynamicImage, palette: &[Lab], opts: &MatchpalOpts) -> ImageBuffer<image::Rgb<u8>, Vec<u8>> {
    let img_rgb = src_img.into_rgb8();
    let pixels: Vec<_> = img_rgb.enumerate_pixels().collect();
    let new_pixels: Vec<_> = pixels.par_iter().filter_map(|(x, y, pixel)| {
        let pixel_rgb = pixel.to_rgb();
        let src_lab = {
            let pixel_channels = pixel_rgb.channels();

            rgb8_components_to_lab(pixel_channels[0], pixel_channels[1], pixel_channels[2])
        };

        let dest_lab = if !opts.no_blend_palette {
            closest_pixel_blended(&src_lab, palette, !opts.no_preserve_lightness, opts.distance_method)
        } else {
            closest_pixel(&src_lab, palette, !opts.no_preserve_lightness, opts.distance_method)
        };

        // make new color...
        let dest_rgb = Srgb::from_color(dest_lab);

        // ...and assign it
        debug!("pixel before: {:?}", pixel);
        debug!("source pixel ({}, {}) {:?} changed to {:?}", x, y, src_lab, dest_lab);
        let new_r = (dest_rgb.red * u8::MAX as f32) as u8;
        let new_g = (dest_rgb.green * u8::MAX as f32) as u8;
        let new_b = (dest_rgb.blue * u8::MAX as f32) as u8;
        let channels = [new_r, new_g, new_b];
        let new_pixel = image::Rgb::from_slice(&channels);
        debug!("pixel after: {:?}", &new_pixel);

        Some((x, y, *new_pixel))
    }).collect();

    debug!("creating new image buffer");
    let mut new_img = ImageBuffer::new(img_rgb.width(), img_rgb.height());
    for (x, y, pixel) in new_pixels {
        debug!("setting pixel ({}, {}) to {:?}", x, y, pixel);
        new_img.put_pixel(*x, *y, pixel)
    }

    new_img
}

fn closest_pixel_blended(src_lab: &Lab, palette: &[Lab], preserve_lightness: bool, distance_method: DistanceMethod) -> Lab {
    // sort palette and pair with color distances (greatest distance first, so `pop` returns small
    // distances)
    let mut sorted_palette = {
        let to_color_distance_pair = |&color| (color, get_color_distance(color, *src_lab, distance_method));
        let mut v: Vec<_> = palette.iter().map(to_color_distance_pair).collect();
        v.sort_by(compare_color_distance_pairs);
        v.reverse();

        v
    };

    let first = sorted_palette.pop().unwrap();
    let first_rgb = Srgb::from_color(first.0);
    if let Some(second) = sorted_palette.pop() {
        let second_rgb = Srgb::from_color(second.0);

        // how much the first color applies to the blended value
        let first_weight = second.1 / (first.1 + second.1);
        let second_weight = 1.0 - first_weight;

        let blended = Lab::from_color(Srgb::new(
                first_rgb.red * first_weight + second_rgb.red * second_weight,
                first_rgb.green * first_weight + second_rgb.green * second_weight,
                first_rgb.blue * first_weight + second_rgb.blue * second_weight,
        ));

        // restore lightness from src image pixel, if wanted
        if preserve_lightness {
            Lab::new(src_lab.l, blended.a, blended.b)
        } else {
            blended
        }
    } else {
        // if no second color exists, just use the first closest color,
        // restoring lightness from src image pixel, if wanted
        if preserve_lightness {
            Lab::new(src_lab.l, first.0.a, first.0.b)
        } else {
            first.0
        }
    }
}

fn closest_pixel(src_lab: &Lab, palette: &[Lab], preserve_lightness: bool, distance_method: DistanceMethod) -> Lab {
    let closest_color =
        palette.iter().map(|&c| color_to_color_distance_pair(c, *src_lab, distance_method)).min_by(compare_color_distance_pairs).unwrap().0;

    // restore lightness from src image pixel, unless set not to
    if preserve_lightness {
        Lab::new(src_lab.l, closest_color.a, closest_color.b)
    } else {
        closest_color
    }
}

fn get_color_distance(color1: Lab, color2: Lab, method: DistanceMethod) -> f32 {
    match method {
        DistanceMethod::Lab => color1.get_color_difference(&color2),
        DistanceMethod::Ab => ((color2.a - color1.a).powi(2) + (color2.b - color1.b).powi(2)).sqrt(),
        DistanceMethod::Hue => get_hue_distance_from_lab(color1, color2),
        DistanceMethod::HueSaturation => get_hue_saturation_distance_from_lab(color1, color2),
    }
}


fn get_hue_distance_from_lab(color1: Lab, color2: Lab) -> f32 {
    use palette::encoding::srgb::Srgb;
    let hsl1 = Hsl::<Srgb>::from_color(color1);
    let hsl2 = Hsl::<Srgb>::from_color(color2);

    get_hue_distance(hsl1, hsl2)
}

fn get_hue_distance(hsl1: Hsl, hsl2: Hsl) -> f32 {
    let raw_diff = (hsl2.hue - hsl1.hue).to_positive_degrees();
    (raw_diff).abs().min((360.0 - raw_diff).abs())
}

fn get_hue_saturation_distance_from_lab(color1: Lab, color2: Lab) -> f32 {
    use palette::encoding::srgb::Srgb;
    let hsl1 = Hsl::<Srgb>::from_color(color1);
    let hsl2 = Hsl::<Srgb>::from_color(color2);

    // max distance is (should be) 180, so normalize to [0, 1]
    let hue_distance = get_hue_distance(hsl1, hsl2) / 180.0;
    let saturation_difference = hsl2.saturation - hsl1.saturation;

    (hue_distance.powi(2) + saturation_difference.powi(2)).sqrt()
}

fn color_to_color_distance_pair(original_color: Lab, other_color: Lab, distance_method: DistanceMethod) -> (Lab, f32) {
    (original_color, get_color_distance(original_color, other_color, distance_method))
}

fn compare_color_distance_pairs(pair1: &(Lab, f32), pair2: &(Lab, f32)) -> Ordering {
    // this is trash but it's gonna work for now until total_cmp is stabilized
    let diff1 = pair1.1;
    let diff2 = pair2.1;
    diff1.partial_cmp(&diff2).unwrap_or(Ordering::Equal)
}

/// Parses a palette from the file path.
///
/// Palette files consist of RGB hex colors separated by lines. For example:
///
/// ```
/// 000000
/// ff0000
/// ffaa00
/// 00a0a0
/// ```
fn parse_palette<P: AsRef<Path>>(file_path: P) -> Palette {
    let file_content = std::fs::read_to_string(file_path).unwrap();
    let lines = file_content.lines();
    let mut palette = Palette::new();
    for original_line in lines {
        let line = original_line.replace(|c: char| !c.is_ascii_hexdigit(), "");
        debug!("parsing line: {}", line);

        let hex: u32 = match u32::from_str_radix(&line, 16) {
            Ok(h) => {
                // if the string isn't 6 chars long, warn the user their color may not be what they expect
                if line.len() != 6 {
                    eprintln!("warning: {} isn't 6 characters long (rgb). the resulting color may not be what you expect?", line);
                }

                h
            }
            Err(e) => {
                eprintln!("warning: couldn't parse `{}` as a color: {}", original_line, e);
                continue
            }
        };
        let color = Srgb::from_u32::<Argb>(hex);
        debug!("parsed color: {:?}", color);
        palette.push(rgb8_components_to_lab(color.red, color.green, color.blue));
    }

    palette
}

fn rgb8_components_to_lab(r: u8, g: u8, b: u8) -> Lab {
    let r = r as f32 / u8::MAX as f32;
    let g = g as f32 / u8::MAX as f32;
    let b = b as f32 / u8::MAX as f32;

    let src_rgb: Srgb<f32> = Rgb::new(r, g, b);

    Lab::from_color(src_rgb)
}

/// Change the colors of an image to match a palette
#[derive(Parser)]
#[clap(version = "0.2.2", author = "Harrison Thorne")]
struct MatchpalOpts {
    /// Input image path
    #[clap(short, long)]
    input: String,

    /// Output image path
    #[clap(short, long, default_value = "matchpal_output.jpg")]
    output: String,

    /// Palette file path
    #[clap(short, long)]
    palette: String,

    /// Don't blend palette colors for smoother images
    #[clap(long)]
    no_blend_palette: bool,

    /// Don't preserve lightness values from the original image
    #[clap(long)]
    no_preserve_lightness: bool,

    /// Which attributes to use when calculating color distance
    #[clap(short, long, default_value = "lab")]
    distance_method: DistanceMethod,
}

#[derive(Copy, Clone)]
enum DistanceMethod {
    Lab,
    Ab,
    Hue,
    HueSaturation,
}

impl FromStr for DistanceMethod {
    type Err = DistanceMethodParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "lab" => Ok(DistanceMethod::Lab),
            "ab" => Ok(DistanceMethod::Ab),
            "hue" => Ok(DistanceMethod::Hue),
            "hue-saturation" | "hs" => Ok(DistanceMethod::HueSaturation),
            _ => Err(DistanceMethodParseError {
                input: s.to_string()
            }),
        }
    }
}

#[derive(Debug)]
struct DistanceMethodParseError {
    input: String
}

impl std::fmt::Display for DistanceMethodParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "distance method not recognized: {}", self.input)
    }
}

impl std::error::Error for DistanceMethodParseError {}

