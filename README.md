# matchpal

**Make any image match any palette.**

I was inspired to make this by some post on r/unixporn, but for the life of me,
I can't find it again. Anyways, I introduce `matchpal`! Short for "match
palette".

## Install it!

Clone it!

```
git clone https://codeberg.org/harrisonthorne/matchpal.git
```

Build it! (Make sure Rust and `cargo` are installed correctly)

```
cd matchpal
cargo install --path .
```

Use it!

```
matchpal --input input_image.jpg --output matchpal_output.jpg --palette colors.palette
```

or

```
matchpal -i input_image.jpg -o matchpal_output.jpg -p colors.palette
```

*Be amazed!* Or disappointed. Results may vary.

## Palette files!

`matchpal` can use any palette you throw at it. Do it!

A palette file is just a bunch of hex colors separated by lines.

Solarized!

```
#002b36
#073642
#586e75
#657b83
#839496
#93a1a1
#eee8d5
#fdf6e3
#dc322f
#cb4b16
#b58900
#859900
#2aa198
#268bd2
#6c71c4
#d33682
```

Nord!

```
#2e3440
#3b4252
#434c5e
#4c566a
#d8dee9
#e5e9f0
#eceff4
#8fbcbb
#88c0d0
#81a1c1
#5e81ac
#bf616a
#d08770
#ebcb8b
#a3be8c
#b48ead
```

Just red and blue! (actually produces really cool results)

```
#ff0000
#0000ff
```

Pound signs are optional!

## Never Asked ~~Questions~~!

### "Wow, this produces some great results!"

Thank you, I'm very flattered! `matchpal` preserves the original lightness value
of each pixel in the image by using L\*a\*b\* color space. My hope was that it
would preserve every image's contrast and character even after modifying colors.

You can turn this off with a `--no-preserve-lightness` flag. With this flag, the
colors from the palette will be used unmodified.

Also, `matchpal` blends the *two* closest colors in the palette-- based on their
distance from the source pixel-- to create a new pixel in the resulting image.
If you don't want to blend colors from the palette, you can use
`--no-blend-palette`.

### "This project stinks! Your coding stinks!"

Then contribute, you buffoon!

(Pull requests and issue reports are very welcome; this product may be far from
perfect)

## Donate!

Want to help support this project? *Don't!* You have your own expenses to take
care of!

(This product is provided free-of-charge, forever and always; credit is
appreciated :))
